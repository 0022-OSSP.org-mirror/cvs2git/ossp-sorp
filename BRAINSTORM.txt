
OSSP sorp BRAINSTORMING
=======================

Currently:
- PESP: Apache+mod_rewrite+MySQL+mod_proxy
- IT Intranet: Apache+mod_rewrite+mod_proxy
- IS Intranet: Apache+mod_php+MySQL

Purposes:
- HTTPS to HTTP gateway
- HTTPS sticky pass-through
- user single-sign on & sticky authentication (user-id|client-cert -> session id)
- remote session tracking (session id -> user id + information) 
- load balancing over multiple backend servers
- backend response caching

Possibilities:
- Apache+mod_rewrite+mod_proxy+?
- Pound: http://www.apsis.ch/pound/
- Squid?
- Apache mod_proxy_add_forward: http://develooper.com/code/mpaf/
  Apache mod_rpad: http://stderr.net/apache/rpaf/
- Stunnel
- Symbion SSL Proxy: http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/sslproxy/

Libraries:
- libcurl
- pth
- sa
- cfg

Prerequisites:
- polling abstraction library (kqueue, poll, select, /dev/poll, etc) for Pth and stand-alone
- SSL support for OSSP sa

Applications:
- NMIT PESP web portal frontend server
- IT Operations Intranet frontend server
- IS Operations North Europe Intranet frontend server
  - MOF
  - ChangeLog
  - Team.xmldb
  - ChangeLog

Tables
======

Session
-------
session_id       /* unique session id */
session_expire   /* expire time of session */
session_scope    /* scope/grade/level of session (login, visit, etc.) */
account_id       /* unique account id (attached user account) */

Account
-------
account_id       /* unique account id */
account_pw       /* password of account */
account_name     /* realname of account holder */
account_location /* realname of account holder */
...
groups...

Possible Approaches
===================

1. Extending SQUID+unofficial patches
   Squid selbst kann Reverse Proxy spielen, aber kein Content Rewriting.
   Fuer Content Rewriting gibt es aber unofficial Patches. Wir muessten
   also "nur" die Sign-On Funktionalitaet einbauen. Allerdings weisz ich
   nicht wie gut Squid bei HTTPS als _ORIGIN_ Server ist.

2. Extending Apache2.0+mod_proxy+mod_ssl
   mod_proxy kann Reverse Proxy spielen, mod_ssl HTTPS reden und der
   Kernel kann Content-Rewriting. Wir muessten also "nur" die Sign-On
   Funktionalitaet einbauen als ein mod_sorp. Allerdings ist der Apache
   2.0 ein dermassener Dreck IMHO (siehe angehaengte Mail), dasz dieser
   Ansatz zwar sexy klingt, aber irgendwie mir gar nicht recht gefaellt.

3. Extending Pound
   Pound ist ein recht junger Pthread-basierter HTTP/HTTPS Reverse
   Proxy, der recht klein und schnuckelig ist und auch leicht
   erweiterbar. Wir muessten das Content-Rewriting und die Sign-On
   Funktionalitaet einbauen. Allerdings bin ich mir bei der HTTP
   Compliance von Pound nicht so recht sicher.

4. Writing OSSP sorp
   Das ist der ambitionierteste Ansatz. Allerdings auch der sauberste
   und zukunftstraechtigste. Insbesondere weil wir im OSSP Projekt
   sowieso eine HTTP Komponente brauchen. Ausserdem haben wir mit den
   bereits existierenden OSSP Libraries schon fast alle Komponenten
   beisammen, die wir brauchen um eine eigene Loesung ohne zu groszen
   Aufwand stemmen zu koennen.

